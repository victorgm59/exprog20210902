package fp.daw.exprog20210902;

import java.util.Comparator;

public class comparador implements Comparator<Automovil> {

	@Override
	public int compare(Automovil o1, Automovil o2) {
		
		int compare = o1.getTipo().compareTo(o2.getTipo());
		if (compare == 0)
			compare = o1.getPotencia().compareTo(o2.getPotencia());
		return compare;
	}


}
