package fp.daw.exprog20210902;

import java.util.Deque;
import java.util.LinkedList;
import java.util.Queue;

public class Ejercicio4 {

	public static void main(String[] args) {

		Queue<Integer> numeros = new LinkedList<Integer>();
		numeros.add(1);
		numeros.add(8);
		numeros.add(7);
		numeros.add(2);
		numeros.add(9);
		numeros.add(18);
		numeros.add(12);
		numeros.add(0);

		System.out.println("Tope -> " + numeros);
		ordenar(numeros);
		System.out.println("Tope -> " + numeros);
	}

	public static void ordenar(Queue<Integer> numeros) {
		Queue<Integer> aux = new LinkedList<Integer>();
		int n = numeros.size();
		for (int i = 0; i < n; i++)
			numeros.offer(aux.poll());
		while (n % 2 !=0 && !aux.isEmpty()) {
			numeros.offer(aux.poll());
			numeros.offer(numeros.poll());
			
		}
	}
}