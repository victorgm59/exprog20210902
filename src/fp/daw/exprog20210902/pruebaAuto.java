package fp.daw.exprog20210902;

import java.util.Arrays;

public class pruebaAuto {

		public static void main(String[] args) {
			Automovil [] automoviles = new Automovil[5];
			automoviles[0] = new Automovil("Hyundai", "Kona", "7519LTS", 2012, Tipo.DIESEL, 204);
			automoviles[1] = new Automovil("Scoda", "Fabia", "5197HTN", 2002, Tipo.ELECTRICO, 141);
			automoviles[2] = new Automovil("Renault", "c5", "3355KXL", 2016, Tipo.HIBRIDO, 306);
			automoviles[3] = new Automovil("Opel", "Corsa", "2159DLX", 2010, Tipo.DIESEL, 150);
			automoviles[4] = new Automovil("Seat", "Ateca", "5973JST", 2022, Tipo.ELECTRICO, 83);
			
			System.out.println(Arrays.toString(automoviles));
			Arrays.sort(automoviles, new comparador());
			System.out.println(Arrays.toString(automoviles));
			Arrays.sort(automoviles);
			System.out.println(Arrays.toString(automoviles));
		}

	}

